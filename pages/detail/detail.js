// pages/detail/detail.js

import { userInfoAuth } from '../../api/api'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    login:false,
    userInfo:{}
  },

  hanldInfo(e) {
    console.log(e.detail.cloudID)
    if (!e.detail.cloudID) {
      wx.showModal({
        title: '提示',
        content: '请授权小程序',
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  },

  handleFocus() {

    //弹出登录窗口
    //    http://127.0.0.1:7001/api/mp/login
    /*wx.login({
      success(res){
        wx.request({
          url:"http://127.0.0.1:7001/api/mp/login",
          data:{
            code:res.code
          },
          success(res){
            console.log(res)
          }
        })
      }
    })*/

    this.setData({ login:true })

    wx.showModal({
      title: '提示',
      content: '请点击上方评论两字授权小程序进行评论',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })


    // wx.getSetting({
    //   success(res){

    //     if (!res.authSetting['scope.userInfo']) {
    //       wx.authorize({
    //         scope: 'scope.userInfo',
    //         success (res) {
    //           // 用户已经同意小程序使用录音功能，后续调用 wx.startRecord 接口不会弹窗询问
    //           //wx.startRecord()
    //           console.log(res)
    //         },
    //         fail(){
    //           alert('123')
    //         },
    //         complete(){
    //           alert(123)
    //         }
    //       })
    //     }
    //   }
    // })


    // wx.authorize({
    //   scope:"scope.userInfo",
    //   success(res){
    //     console.log(res)
    //   }
    // })

    // wx.getUserInfo({
    //   success:function(res){
    //     console.log(res)
    //   }
    // })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options.lid)
    
    //1.
    userInfoAuth().then((res)=>{ this.setData({  userInfo : res }) })
    //2.
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})