//index.js
//获取应用实例
const app = getApp()

import { showCanvas,debounce } from '../../utils/util'
import { indexList,getLocation } from '../../api/api'

Page({

  data: {
    isScroll: false,
    menu: false,
    hide: true,
    topNum: 0,
    data:[
      {lid:0,title:"测试",}
    ],
    percent:0
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  handleScroll(e)  {

    

    var scrollTop = e.detail.scrollTop

    var scrollHeight = e.detail.scrollHeight
    
    if (scrollTop > 300) {

      this.setData({
        isScroll: true
      })
    } else {
      this.setData({
        isScroll: false
      })
    }
    //滚动的高度/(容器的总高度+小程序获取滚动滑块的高度)
    //console.log(scrollTop/scrollHeight)
    var res = wx.getSystemInfoSync()
    
    this.setData({ percent : ( scrollTop  / (scrollHeight - res.windowHeight) )*100 })
  },

  handleClick(e) {
    //触发将其样式改变
    this.setData({
      hide: false
    })
    this.setData({
      menu: true
    })
    //console.log(this.data.hide)

  },
  onClose() {
    this.setData({
      hide: true
    })
    //this.setData({menu:false})
  },
  handleTop() {
    //使用scroll-view就不能使用wx.pageScrollTo
    this.setData({
      topNum: 0
    })
  },
  handleArrow() {

    var res = wx.getSystemInfoSync()
    //console.log(res.windowHeight)
    this.setData({
      topNum: res.windowHeight
    })
  },
  onLoad: function () {
    //1.
    showCanvas(wx)

    //2.获取文章信息
    //http://127.0.0.1:7001/api/index

    //indexList().then((res)=>{this.setData({ data:res.res })})

    //3.地理
    getLocation()

    //4.系统信息
    var data = wx.getStorageSync('systemInfo')
    console.log(data)


  },

  getUserInfo: function (e) {
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
  
 
   
})