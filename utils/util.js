const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


const showCanvas = () => {

  //绘制canvas id ：mycanvas
  /** 
   * 大小随机
   * 数量书记
   * 颜色随机
   * 消失随机
   * 定时器
   * 
   * 画笔到下方-》画出一个圆(自动闭合) -》设置背景透明度
   */

  //画出50个园 园的x方向的偏移量范围0-res.windowWidth
  //半径大小 1 - 7
  //不停的重排重绘  小球逐渐上升改变y抽的距离
  /*var ctx = wx.createCanvasContext('mycanvas')
  var res = wx.getSystemInfoSync()
 
  var spd = []

  function gameloop(){
    var bgColor = "#fff"
  
    //ctx.setLineWidth(5)
    ctx.setFillStyle(bgColor)
    ctx.setGlobalAlpha(0.3)
    ctx.setStrokeStyle('red')
    
    var offsetX = []
    var radius = []
    //console.log(requestAnimationFrame)
    for(var i=0;i<25;i++){
      spd[i] = Math.random() * 0.017 + 0.03;
      console.log(spd)
      offsetX[i] = Math.floor(Math.random() * res.windowWidth)
      radius[i] = Math.floor(Math.random()*7 + 1)
      ctx.moveTo(2 * radius[i] + offsetX[i],res.windowHeight - radius[i])
      ctx.arc(radius[i] + offsetX[i],res.windowHeight - radius[i] - spd[i],radius[i],0,2 * Math.PI,true)
    }
    //ctx.stroke()
    ctx.fill()
    ctx.draw()

    //requestAnimationFrame(gameloop)
  }

  gameloop()*/

  var global = new Global()

  global.init()

  function gameloop(){
    global.draw()
    //requestAnimationFrame(gameloop)
  }

  gameloop()
}


class Global {
  alive = []
  radius = []
  offsetX = []
  offsetY = []
  spd = []
  num = 25

  constructor() {
    // this.alive = true
    // this.radius = 0
    // this.offsetX = 0
    // this.offsetY = 0
    // this.spd = Math.random() * 0.017 + 0.03
  }
  init() {

    for (var i = 0; i < this.num; i++) {
      this.alive[i] = true
      this.offsetX[i] = 0
      this.offsetY[i] = 0
      this.radius[i] = 0
      this.spd[i] = Math.random() * 0.017 + 0.03
    }

  }

  draw() {
    //放大缩小的范围0-0.5
    //起点 半径 
    // ctx.arc(radius + offsetX, res.windowHeight - radius - offsetY, radius, 0, 2 * Math.PI, true)
    var ctx = wx.createCanvasContext('mycanvas')

    var res = wx.getSystemInfoSync()
    for (var i = 0; i < this.num; i++) {

      var offsetX = Math.floor(Math.random() * res.windowWidth)
      var bgColor = "#fff"

      ctx.setFillStyle(bgColor)
      ctx.setGlobalAlpha(0.3)

      if (this.alive[i]) {
        //半径 速度
        if (this.radius[i] < 7) {
          this.radius[i] += this.spd[i] * 12
        } else {
          this.offsetY[i] += this.spd[i] * 18
        }

      }

      //console.log(this.radius[i])

      ctx.moveTo(2*(this.radius[i] + offsetX),res.windowHeight - this.radius[i] - this.offsetY[i]-this.radius[i])

      ctx.arc(this.radius[i] + offsetX, res.windowHeight - this.radius[i] - this.offsetY[i], this.radius[i], 0, 2 * Math.PI, true)
    }

    ctx.fill()

    ctx.draw()

  }
}

const debounce = (fn,delay)=>{
  var timer = null
  var args = arguments

  return function(){
    timer && clearTimeout(timer)
    timer = setTimeout(function(){
      fn.apply(this,args)
    },delay || 500)
  }
}

module.exports = {
  formatTime: formatTime,
  showCanvas,
  debounce
}