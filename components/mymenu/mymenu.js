// components/mymenu/mymenu.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    menu:{
      type:Boolean,
      value:false
    },
    hide:{
      type:Boolean,
      value:true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // hide:true
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleClick(){
      
      // if(this.data.hide){
        
      //   this.setData({ hide : false })
      // }else{
      //   this.setData({ hide : true })
      // }

      this.triggerEvent('close')
    }
  }
})
