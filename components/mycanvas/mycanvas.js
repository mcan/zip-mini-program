// components/mycanvas/mycanvas.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    productPop(){
      //产生汽包
      let ctx = wx.createCanvasContext('mycanvas',this)
      
      const query = this.createSelectorQuery()
      query.select('#canvas').boundingClientRect()
      query.selectViewport().scrollOffset()
      query.exec((res)=>{
        
      })
      ctx.rect(100,100,50,50)
      // ctx.setFillStyle('red')
      // ctx.fill()
      ctx.stroke()
      ctx.draw()

      
    }
  },
  lifetimes:{
    attached(){
      this.productPop()
    },
    detached(){

    }
  }
})

//自定义组件
