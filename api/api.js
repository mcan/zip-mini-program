//地理授权

const devServer = "http://127.0.0.1:5000"
const onLineServer = ""

const locationAuth = function () {
    wx.getSetting({
        success(res) {
            if (!res.authSetting['scope.userLocation']) {
                wx.showModal({
                    title: "地理位置未授权",
                    content: "如需使用博客，请打开手机中的地位授权，开启后重新打开小程序",
                    showCancel: true,
                    confirmText: "去授权",
                    success(res) {
                        if (res.confirm) {
                            // console.log('用户点击确定')
                            wx.authorize({
                                scope: "scope.userLocation",
                                success(res) {

                                }
                            })
                        } else if (res.cancel) {
                            console.log('用户点击取消')
                        }
                    }
                })

            }
        }
    })

}

//首页数据
const indexList = function () {

    return new Promise((resolve, reject) => {
        wx.request({
            url: `${devServer}/api/index`,
            method: "GET",
            success: ({
                data
            }) => {
                resolve(data)
            }
        })
    })

}

//获取经纬度
const getLocation = function () {
    wx.getSetting({
        success(res) {
            if (res.authSetting['scope.userLocation']) {
                wx.getLocation({
                    type: "wgs84",
                    altitude: true,
                    success(res) {

                        console.log(`${ res.longitude },${ res.latitude }`)
                    }
                })
            }
        }
    })
}

//用户信息授权
const userInfoAuth = function () {
    return new Promise((resolve, reject) => {
        wx.getSetting({
            success(res) {
                if (res.authSetting['scope.userInfo']) {
                    wx.getUserInfo({
                        success(res) {
                            res = JSON.parse(res.rawData)
                            resolve(res)
                        }
                    })
                }
            }
        })
    })


}

module.exports = {
    locationAuth,
    indexList,
    getLocation,
    userInfoAuth
}